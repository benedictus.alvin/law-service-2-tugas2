from django.db import models
from django.contrib.auth.models import *

class Savefile(models.Model):
    upload = models.FileField(upload_to='Files/', null=False)
    url = models.URLField(null=False)

    class Meta:
        db_table = 'Savefile'
